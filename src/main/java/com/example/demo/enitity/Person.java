package com.example.demo.enitity;

import java.util.Calendar;
import java.util.TimeZone;

public class Person {
    private String name;
    private Integer age;

    public Person(String name) {
        setName(name);
        Integer age = Calendar.getInstance(TimeZone.getDefault()).get(Calendar.SECOND);
        setAge(age);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getAge() {
        return age;
    }

}